<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentController extends CI_Controller {

	public function index(){
		$query=$this->db->get('student');
		$studentlist=$query->result();
		$data['studentlist']=$studentlist;

		$this->load->view('student_form',$data);
	}
	public function show()
	{
		
		echo "<h1>This is Hello Controller</h1>";
	}
	public function AddStudent(){
		$name=$this->input->post('name');
		$email=$this->input->post('email');
		$address=$this->input->post('address');
		//echo "$name => $email => $address";
		$student = array(
			'name' => $name , 
			'email' => $email ,
			'address' => $address ,
		    );
		if($this->Mymodel->Add($student,'student')){
			$this->session->set_flashdata('message','ေအာင္ၿမင္စြာ ထည့္သြင္းလိုက္ပါၿပီ ');
		   	redirect('student');
		}
	}

	public function deletestudent(){
			$id=$this->input->post('id');
			$this->Mymodel->Deleterow($id,'student');
		$this->session->set_flashdata('message','ေအာင္ၿမင္စြာ ဖ်က္လိုက္ပါၿပီ ။ ');
			redirect('student');
	}

	public function editstudent(){
			$id=$this->uri->segment(2);

			$student=$this->Mymodel->Edit($id,'student');

			/*foreach ($student as $mystudent) {
				echo $mystudent->id;
				echo $mystudent->name;
			}*/
			$data['student']=$student;
			$this->load->view('student_form',$data);
	}

	public function updatestudent(){
			$id=$this->input->post('id');
			$name=$this->input->post('name');
			$email=$this->input->post('email');
			$address=$this->input->post('address');

				$student = array(
			'name' => $name , 
			'email' => $email ,
			'address' => $address ,
		    );

			$this->Mymodel->Update($id,$student,'student');
		$this->session->set_flashdata('message','ေအာင္ၿမင္စြာ ၿပင္ဆင္လိုက္ပါၿပီ ။ ');
			redirect('student');
	}






















}

