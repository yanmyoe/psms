<!DOCTYPE html>
<html lang="en">
 <?php
        include('head.php');
 ?>
  <body class="app sidebar-mini">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="index.html">Vali</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"></a>
      <!-- Navbar Right Menu-->
    <?php
        include('appnav.php');
    ?>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
   <?php
        include('aside.php');
   ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Student Management System</h1>
          <p>Start a beautiful journey here</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Home</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
           
                  <div class="col-md-10">

<?php
        if($this->session->flashdata('message')){
            $message=$this->session->flashdata('message');

?>
                     <div class="col-md-12">
            <div class="bs-component">
              <div class="alert alert-dismissible alert-success">
                <button class="close" type="button" data-dismiss="alert">×</button><strong>Well done!</strong> 
                <?php
                    echo $message;
                ?>
              </div>
            </div>
          </div>

          <?php
                }
          ?>

          <div class="tile">
            <h3 class="tile-title">Register</h3>
            <div class="tile-body">
         
         <?php
                if(isset($student)){
                    foreach ($student as $mystudent) {
                      $id=$mystudent->id;
                      $name=$mystudent->name;
                      $email=$mystudent->email;    
                      $address=$mystudent->address;
                     
                      }   
         ?>

         <form class="form-horizontal" action="updatestudent" method="POST">
                <div class="form-group row">
                  <label class="control-label col-md-3">Name</label>
                  <div class="col-md-8">
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <input class="form-control" type="text" value="<?php echo $name ?>"  name="name">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Email</label>
                  <div class="col-md-8">
                <input class="form-control col-md-8" type="email" value="<?php echo $email ?>"  name="email">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Address</label>
                  <div class="col-md-8">
                    <textarea class="form-control" rows="4" placeholder="Enter your address" name="address"><?php echo $address ?></textarea>
                  </div>
                </div>

                 <div class="form-group row">
                  
                  <div class="col-md-8">
               <input type="submit" class="btn btn-warning" value="Update">
                  </div>
                </div>
               
       </form>
       <?php
               
             }
             else{
       ?>
       <form class="form-horizontal" action="addstudent" method="POST">
                <div class="form-group row">
                  <label class="control-label col-md-3">Name</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter full name"  name="name">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Email</label>
                  <div class="col-md-8">
                    <input class="form-control col-md-8" type="email" placeholder="Enter email address" name="email">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Address</label>
                  <div class="col-md-8">
                    <textarea class="form-control" rows="4" placeholder="Enter your address" name="address"></textarea>
                  </div>
                </div>

                 <div class="form-group row">
                  
                  <div class="col-md-8">
               <input type="submit" class="btn btn-primary" value="Register">
                  </div>
                </div>
               
              </form>
              <?php
                    }
              ?>
  
            </div>


            
          </div>
        </div>

           <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered table-responsive" id="sampleTable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Update</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                   <?php
                    if(isset($studentlist)){
                      $i=1;
                      foreach ($studentlist as $student) {
                          $id= $student->id;
                           $name= $student->name;
                           $email= $student->email;
                           $address= $student->address;
      echo "<tr>
                  <td>$i</td>
                  <td>$name</td>
                  <td>$email</td>
                  <td>$address</td>
                  <td>
                  <a href='".base_url()."student/$id' class='btn btn-info'>Update</a>
                  </td>
                  <td>
                  <form action='deletestudent' method='post' onsubmit='return confirm(\"Are You Sure?\")'>
                  <input type='hidden' name='id' value=$id>
                  <input type='submit' class='btn btn-danger' value='Delete'>
                  </form>
                  </td>
                  </tr>";
                        $i++;
                      }
                    }

              ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

          </div>
        </div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="<?php echo base_url();?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="<?php echo base_url();?>js/plugins/pace.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>


    <!-- Page specific javascripts-->
    <!-- Google analytics script-->

    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
  </body>
</html>