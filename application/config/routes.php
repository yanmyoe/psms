<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'StudentController';

$route['addstudent']='StudentController/AddStudent';
$route['student']='StudentController';
$route['deletestudent']='StudentController/deletestudent';
$route['student/(:num)']='StudentController/editstudent/$1';
$route['student/updatestudent']='StudentController/updatestudent';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
