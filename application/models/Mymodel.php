<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mymodel extends CI_Model{
   
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     public function Add($data,$table){
     		if($this->db->insert($table,$data)){
     			return true;
     		}else{
     			return false;
     		}
     }
     public function Deleterow($id,$table){
     		$this->db->where('id',$id);
			$this->db->delete($table);
     }
     public function Edit($id,$table){
     	  $query=$this->db->get_where($table,array('id'=>$id));
			$student=$query->result();
			return $student;
     }

     public function Update($id,$data,$table){
     	$this->db->where('id',$id);
			$this->db->update($table,$data);
			
     }


     		



  }